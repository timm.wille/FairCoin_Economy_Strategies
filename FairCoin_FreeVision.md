# FairCoin FreeVision ( proposal ) Whitepaper

## Introduction
FairCoin exists since 2014 with the vision of a fair and cooperative working economy. All informations about it can find under https://fair-coin.org the base website of FairCoin.

Main part of FairCoin is the price mechanism to stableize the price and prevent the pump and dump and the effects to the fair economy.

Another important part is the proof-of-cooperation algorithm where  specific nodes create the blocks within all transactions by a round-robin procedure and almost without a fee. This proof-of-cooperation algorithm saves ressources and scales up better than a proof-of-work or proof-of-stake system as other cryptocurrencies use.

The reality shows us that the existing price mechanism is very limited and creates some needs that is hard to fulfill. In principle the system depends on investors that put new capital into the system that secures the liquidity that is necessary to hold the price stable and on level.
During the creation of this whitepaper the current market price of FairCoin is 0.10€ but the official price is still at 1.20€

FairCoin FreeVision don't want to call the price mechanism in question but want add another price mechanism that works everytime and in every market and doesn't depends on new investors or capital.

We want ADD other possibilities to FairCoin but we don't want and will NEVER fork the FairCoin blockchain. We don't want compete against the existing mechanism and infrastructure but we want also support it and maybe we can improve the whole FairCoin ecosystem with our ideas.

We see FairCoin at the moment primary driven und decided by Fair.Coop as an eco-human-centric but idealistic movement.
But we see also similar but other movements like komun.org as an eco-human-centric but with a higer focus to decentralization.

We think that if we want to create a powerful cryptocurrency that could become a alternative to fiat money or Bitcoin then we need to spread FairCoin and invite people that for example want support a fair economy but don't want to go the idealistic way but prefer more pragmatism and realism.
We want make compromises instead to follow only the one way if it brings us a step forward.

FairCoin FreeVision should be understood as a part of FairCoin community that don't want ignore the free market but work with it and find a middle-way between liquidity and stability.
We also want think about decentralization, about possible improvements of the proof-of-cooperation algorithm or the way to process the exchanges from fiat to FairCoin or FairCoin to fiat.

FairCoin FreeVision is made for eco-libertarians or progessivists.


## The price mechanism

Price adjustments should be not made by humans in assemblies because we think that it works only in a very low level but cant portray the global movement or community of thousands of FairCoin members.
It is crazy that a dozen of members decides about the new price but the community has 500 or more members.

We think that the price adjustments should be done by automatism.

Our support price will calculated by this formula

    support price = integer( ( free market price – 0.01€ ) * 10 ) / 10

Examples: ( market price / our support price )
* 0.10€ / 0.05€
* 0.11€ / 0.10€
* 0.12€ / 0.10€
* ..
* 0.20€ / 0.10€
* 0.21€ / 0.20€
* ..
* 0.30€ / 0.20€
* 0.31€ / 0.30€

The support price is everytime lower than the free market price. So we can prevent arbitrage trading and the liquidity problematic.


## The exchange system

We change OtherCurrencies to FairCoin to free market price but FairCoin to OtherCurrencies only to support price.

Example:
If the market price is 0.20€ we change FairCoin to Other to 0.10€ but Other to FairCoin to 0.20€

So arbitrage trading can prevented and in opposite the service itself do arbitrage trading and the win of aribtrage trading will go to the existing FairCoin system and supports the official price of 1.20€ or we invest the win into technical improvements of FairCoin where all FairCoiner will profit.

Our exchange system will be open and transparent for everyone ( with respect to privacy to the members ) and processed by p2p exchanges made by authorized supporters.

### The slot system

To make the exchanges efficient to handle we uses a slot system to organize all exchanges.

Every slot has same size for example 100€. So we would support only exchanges in 100€ steps.

The maximum number of slots will calculated by promised slots from supporters.

To decentralize it it is possible that small FairCoiner can become a supporter and for example support only 1 slot per month.
The risk is very low for all supporters because of the price spread.

Every user ( collective or individual ) can request an exchange and the system calculates a score and assigns the slots to the requestors.

Merchants and companies first, individuals last because we don't want support individual speculations and think that every individual should only buy so much FairCoin he could lose in worst case.

Users with lower amount of assigned slots in past will prefered but all users will be respected in the calculation. 
